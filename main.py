# Crash
import random

money = 10000;
bet = 0;
multiplier = 1;
value = 0;
playing = True;

print("Willkommen bei Crash");
print(f"\nKontostand: {money}\n")

while playing:

    while True:
        print("Wie viel möchtest du bieten?");
        print("Eingabe: ", end="");
        bet = input()
        if bet.lower() == "all":
            bet = money;
        else:
            bet = int(bet);
        if bet <= money:
            money -= bet;
            multiplier = 1;
            break;

    while True:
        print(f"\nDein Einsatz: {bet}€");
        print(f"Der Multiplikator liegt bei: x{multiplier}");
        print("\nErhöhen (+) | Aussteigen (-)");
        if input() == "+":
            if random.randint(0, 1) != 0:
                multiplier += 0.5;
                if multiplier == 10:
                    i = bet * multiplier;
                    money += i;
                    print("Maximalwert von x10 erreicht.")
                    print(f"Du bist mit {i}€ automatisch ausgestiegen.")
                    print(f"Neuer Kontostand: {money}€")
                    break;
            else:
                print("// CRASH //");
                print(f"Du hast {bet}€ verloren.")
                print(f"Neuer Kontostand: {money}€\n")
                break;
        else:
            i = bet * multiplier;
            money += i;
            print(f"Du bist mit {i}€ ausgestiegen.")
            print(f"Neuer Kontostand: {money}€")
            break;

    if money != 0:
        print("Weiter spielen (+) | Casino verlassen (-)")
        if input() == "-":
            playing = False;
    else:
        playing = False;